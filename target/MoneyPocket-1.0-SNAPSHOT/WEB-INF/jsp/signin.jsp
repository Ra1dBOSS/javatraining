<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome</title>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="jumbotron">
    <h1><a href="/">Welcome to Money Pocket!</a></h1>
    <p>Probably the best wallet in the world!</p>

    <form:form name="signin" method="POST" modelAttribute="registrationForm" >
        Login
        <input type="text" name="login">
        Password
        <input type="password" name="password">
        <input type="submit" value="Signin" >
    </form:form>
</div>
</body>
</html>