<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
 <head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
     <title>Bills</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

 </head>
<body>
 <section>
     <div class="jumbotron">
         <div class="container">
             <h1><a href="/main">Welcome to Money Pocket!</a></h1>
             <p>Bills from user  <a href="/user_info">${user.login}</a><a href="/logout">(logout)</a>.</p>
         </div>
     </div>
 </section>

 <section class="container">
     <c:forEach var="bill" items="${bills}">
     <div class="row">
         <div class="col-sm-6 col-md-3" style="padding-bottom: 15px">
             <div class="thumbnail">
                 <div class="caption">
                 <h3>${bill.name}</h3>
                     <p>Card Number:${bill.cardNumber}</p>
                     <p>Card Holder:${bill.cardHolder}</p>
                     <p>Card Expiration Date:${bill.expirationDate}</p>
                     <p>Available ${bill.balance} USD</p>
                     <p>
                     <form action="/bills/delete" method="get">
                         <input type="hidden" name="dishId" value="${bill.id}"  id="hidden_field" />
                         <input class="btn btn-warning btn-large" type="submit" value="Delete" id="frm1_submit" />
                     </form>
                     </p>
                     <p>
                         <input href="" value="Transactions" class="btn btn-warning btn-large">
                     </p>
                </div>
             </div>
         </div>
     </div>
     </c:forEach>
 </section>

</body>
</html>
