<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
    <title>Bills</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1><a href="/main">Welcome to Money Pocket!</a></h1>
            <p>User information about ${user.login}<a href="/logout">(logout)</a> .</p>
        </div>
    </div>
</section>

<section class="container">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <div class="caption">
                        <p>Name:${user.name}</p>
                        <p>Lastname:${user.lastname}</p>
                        <p>Surname:${user.surname}</p>
                        <p>Login:${user.login}</p>
                        <p><a href="/user_info_edit">
                            <input type="submit" value="Edit" class="btn btn-warning btn-large">
                        </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
</section>

</body>
</html>
