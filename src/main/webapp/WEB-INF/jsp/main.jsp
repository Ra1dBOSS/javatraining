<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome</title>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
</head>
<body>
<div class="jumbotron">
    <h1><a href="/main">Welcome to Money Pocket!</a></h1>
    <p> Greeting, <a href="/user_info">${user.login}</a><a href="/logout">(logout)</a>.</p>

    <form action="/bills" method="get">
        <input type="submit" value="Get the name of all bills" id="frm1_submit" />
    </form>
    <p></p>
    <form action="/bill_binding" method="get">
    <input type="submit" value="Add new bill" id="frm2_submit" />
    </form>
    <p></p>
    <form action="/bill_info" method="get">
        Name
        <input type="text" name="name" >
        <input type="submit" value="Find bill by name" id="frm3_submit" />
    </form>
</div>
</body>
</html>
