<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
    <title>Bills</title>
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">

</head>
<body>
<section>
    <div class="jumbotron">
        <div class="container">
            <h1><a href="/main">Welcome to Money Pocket!</a></h1>
            <p>Add new bill to <a href="/user_info">${user.login}</a><a href="/logout">(logout)</a>.</p>
        </div>
    </div>
</section>

<section class="container">
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
                <div class="caption">
                    <form:form name="bill_binding" method="POST" modelAttribute="billForm" >
                        <p>Name:</p>
                        <p><input type="text" name="name"></p>
                        <p>Card Number:<input type="text" name="cardNumber"></p>
                        <p>Card Holder:<input type="text" name="cardHolder"></p>
                        <p>Expiration Date:<input type="date" name="expirationDate"></p>
                        <p>CVC:</p>
                        <p><input type="text" name="cvc"></p>
                        <p>Balance:</p>
                        <p><input type="text" name="balance"></p>
                        <input type="submit" value="Add" class="btn btn-warning btn-large" >
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>
