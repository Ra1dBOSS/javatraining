<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome</title>
    <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
  </head>
  <body>
  <div class="jumbotron">
    <h1><a href="/"> Welcome to Money Pocket! </a></h1>
    <p> Probably the best wallet in the world! </p>

    <table >
      <tr>
        <th>
          <form action="/signin" method="get">
            <input type="submit" value="Sign In" id="frm1_submit" />
          </form>
        </th>
        <th>
          <form action="/registration" method="get">
            <input type="submit" value="Registration" id="frm2_submit" />
          </form>
        </th>
      </tr>
    </table>
  </div>
  </body>
</html>
