package com.vironit.moneypocket.view.console;

public interface MenuInterface {

    public void mainScreenConsoleShow();

    public int mainMenu();

}
