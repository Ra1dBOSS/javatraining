package com.vironit.moneypocket.view.console;

import com.vironit.moneypocket.service.implementation.HomeServiceImpl;
import com.vironit.moneypocket.loggers.ControllerLogger;
import com.vironit.moneypocket.model.exceptions.BusyLoginException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.IncorrectPasswordException;
import com.vironit.moneypocket.model.exceptions.IncorrectPersonalInformationException;

import java.util.Scanner;


public class MainConsoleMenuConsoleView extends AbstractConsoleMenu {

    private HomeServiceImpl homeServiceImpl;
    private ControllerLogger logger;

    public MainConsoleMenuConsoleView(HomeServiceImpl homeServiceImpl) {
        this.homeServiceImpl = homeServiceImpl;
        logger = new ControllerLogger();
    }

    public void mainScreenConsoleShow() {
        System.out.println("Money Pocket");
        System.out.println("1.login");
        System.out.println("2.registration");
        System.out.println("0.exit");
    }

    public void registration() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Name:");
        String name = scanner.nextLine();
        System.out.print("Surname:");
        String surname = scanner.nextLine();
        System.out.print("Lastname:");
        String lastname = scanner.nextLine();
        System.out.print("Login:");
        String login = scanner.nextLine();
        System.out.print("Password:");
        String password = scanner.nextLine();
        try {
            homeServiceImpl.registration(login, password, name, surname, lastname);
            System.out.println("Succesful registration");
        } catch (IncorrectPasswordException e) {
            logger.getLogger().info("IncorrectPassword", e);
            System.out.println("Incorrect Password (It should consist of 8 characters and contain numbers and Latin characters)");
        } catch (BusyLoginException e) {
            logger.getLogger().info("BusyLogin", e);
            System.out.println("This login is busy");
        } catch (IncorrectPersonalInformationException e) {
            logger.getLogger().info("IncorrectPersonalInformation", e);
            System.out.println("Invalid personal information");
        }
    }

    public boolean login() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Login:");
        String login = scanner.nextLine();
        System.out.print("Password:");
        String password = scanner.nextLine();
        try {
            homeServiceImpl.login(login,password);
            return true;
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

}
