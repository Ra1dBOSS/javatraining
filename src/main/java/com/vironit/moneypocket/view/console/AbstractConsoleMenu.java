package com.vironit.moneypocket.view.console;

import java.util.Scanner;

public abstract class AbstractConsoleMenu implements MenuInterface {

    public int mainMenu() {
        int ans = -1;
        while (ans != 0) {
            mainScreenConsoleShow();
            Scanner scanner = new Scanner(System.in);
            try {
                ans = Integer.parseInt(scanner.next());
                return ans;
            } catch (NumberFormatException e) {
                ans = -1;
            }
        }
        return ans;
    }
}
