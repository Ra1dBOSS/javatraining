package com.vironit.moneypocket.view.console;

import com.vironit.moneypocket.service.implementation.HomeServiceImpl;

public class MainConsoleView {

    public static void main(String[] args) {

        HomeServiceImpl homeServiceImpl = new HomeServiceImpl();
        int ans = -1;
        while (ans != 0) {
            MainConsoleMenuConsoleView main = new MainConsoleMenuConsoleView(homeServiceImpl);
            ans = main.mainMenu();
            if (ans == 1) {
                if (main.login()) {
                    UserConsoleMenuConsoleView userMenuConsoleView = new UserConsoleMenuConsoleView(homeServiceImpl);
                    ans = userMenuConsoleView.mainMenu();

                }
            }
            if (ans == 2) {
                main.registration();
            }
        }
        System.out.println("Bye!");
    }

}
