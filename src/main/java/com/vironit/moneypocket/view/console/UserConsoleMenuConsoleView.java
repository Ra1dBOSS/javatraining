package com.vironit.moneypocket.view.console;

import com.vironit.moneypocket.service.implementation.HomeServiceImpl;

public class UserConsoleMenuConsoleView extends AbstractConsoleMenu {

    HomeServiceImpl homeServiceImpl;

    public UserConsoleMenuConsoleView(HomeServiceImpl homeServiceImpl) {
        this.homeServiceImpl = homeServiceImpl;
    }

    public void mainScreenConsoleShow() {
        System.out.println("Money Pocket");
        System.out.println("1.Create new bill");
        System.out.println("2.Get list of bills");
        System.out.println("3.Choose bill");
        System.out.println("0.exit");
    }

}
