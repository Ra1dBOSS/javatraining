package com.vironit.moneypocket.loggers;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ControllerLogger extends AbstractLogger{
    public ControllerLogger() {
        String fileName = "log4j.properties";
        PropertyConfigurator.configure(fileName);
        this.logger = Logger.getLogger("logfile");
    }
}
