package com.vironit.moneypocket.loggers;

import org.apache.log4j.Logger;

public abstract class AbstractLogger {

    Logger logger;

    public Logger getLogger() {
        return this.logger;
    }

}
