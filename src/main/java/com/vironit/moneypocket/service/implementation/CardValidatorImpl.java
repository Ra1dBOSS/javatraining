package com.vironit.moneypocket.service.implementation;

import com.vironit.moneypocket.service.CardValidator;
import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CardValidatorImpl implements CardValidator {

    @Override
    public boolean luhnCheck(String card) {
        if ((card == null) || (card.length() < 16) || (card.length() > 16))
            return false;
        Pattern p = Pattern.compile("^[0-9]*$");
        Matcher m = p.matcher(card);
        if (!m.matches())
            return false;
        int digit = calculateCheckDigit(card);
        return digit % 10 == 0;
    }

    private int calculateCheckDigit(String card) {

        int[] digits = new int[card.length()];
        for (int i = 0; i < card.length(); i++) {
            digits[i] = Character.getNumericValue(card.charAt(i));
        }

        for (int i = digits.length - 2; i >= 0; i -= 2)	{
            digits[i] += digits[i];

            if (digits[i] >= 10) {
                digits[i] = digits[i] - 9;
            }
        }
        int sum = 0;
        for (int i = 0; i < digits.length; i++) {
            sum += digits[i];
        }

        return sum;
    }
}
