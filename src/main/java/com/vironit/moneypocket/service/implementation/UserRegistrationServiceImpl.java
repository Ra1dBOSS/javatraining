package com.vironit.moneypocket.service.implementation;

import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.BusyLoginException;
import com.vironit.moneypocket.model.exceptions.IncorrectPasswordException;
import com.vironit.moneypocket.model.exceptions.IncorrectPersonalInformationException;
import com.vironit.moneypocket.model.exceptions.NullException;
import com.vironit.moneypocket.service.UserRegistrationService;
import com.vironit.moneypocket.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserRegistrationServiceImpl implements UserRegistrationService {

    @Autowired
    UsersService usersService;

    private void passwordCheck(String password) throws IncorrectPasswordException {
        if (password == null) {
            throw new IncorrectPasswordException();
        }
        if (password.length() < 8) {
            throw new IncorrectPasswordException();
        }
        Pattern pattern = Pattern.compile("\\w+");
        Matcher matcher = pattern.matcher(password);
        if (!matcher.matches()) {
            throw new IncorrectPasswordException();
        }
    }

    @Override
    @Transactional
    public void registerNewUser(String login, String password, String name, String surname, String lastname)
                                               throws IncorrectPasswordException,IncorrectPersonalInformationException, BusyLoginException {
        try {
            passwordCheck(password);
            usersService.loginCheck(login);
            User user = new User(login, password, name, surname, lastname);
            usersService.add(user);
        } catch (NullException e) {e.printStackTrace();}
    }
}
