package com.vironit.moneypocket.service.implementation;

import com.vironit.moneypocket.dao.*;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.BusyLoginException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;
import com.vironit.moneypocket.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;


@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersDAO usersDAO;

    @Override
    @Transactional
    public void loginCheck(String login) throws BusyLoginException, NullException {
        User user = null;
        try {
            user = usersDAO.getByLogin(login);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EntityNotFoundException e) {
            throw new BusyLoginException();
        }
        if (user != null)
            throw new BusyLoginException();
    }

    @Override
    @Transactional
    public void add(User user) throws BusyLoginException, NullException {
        this.loginCheck(user.getLogin());
        usersDAO.add(user);
    }

    @Override
    @Transactional
    public User findUser(String login, String password) throws EntityNotFoundException, NullException, IOException {
        User x = usersDAO.getByLogin(login);
        if ((x != null) && (x.getPassword().equals(password))) {
            return x;
        }
        throw new EntityNotFoundException();
    }

    @Override
    @Transactional
    public User findUserByLogin(String login) throws EntityNotFoundException{
        User user = null;
        try {
            user = usersDAO.getByLogin(login);
        } catch (NullException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        } catch (IOException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        }
        return user;
    }

    @Override
    @Transactional
    public void updateUserInformation(User user) throws EntityNotFoundException {
        try {
            if (usersDAO.getById(user.getId()) != null)
                usersDAO.update(user);
        } catch (IOException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        } catch (NullException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        }
    }

    @Override
    @Transactional
    public void deleteUser(User user) throws EntityNotFoundException {
        try {
            if (usersDAO.getById(user.getId()) != null)
                usersDAO.delete(user);
        } catch (NullException e ) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        } catch (IOException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        }
    }
}
