package com.vironit.moneypocket.service.implementation;

import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.*;
import com.vironit.moneypocket.service.HomeService;
import com.vironit.moneypocket.service.UserRegistrationService;
import com.vironit.moneypocket.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    private UsersService users;

    private User currentUser;

    @Autowired
    private UserRegistrationService userRegistrationService;

    public HomeServiceImpl() {
    }

    @Override
    public void registration(String login, String password, String name, String surname, String lastname) throws IncorrectPasswordException, BusyLoginException, IncorrectPersonalInformationException {
        userRegistrationService.registerNewUser(login, password, name, surname, lastname);
    }

    @Override
    public User login(String login, String password) throws EntityNotFoundException {
        currentUser = null;
        try {
            currentUser = users.findUser(login, password);
        } catch (NullException e) {
            throw new EntityNotFoundException();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (currentUser == null)
            throw new EntityNotFoundException();
        return currentUser;
    }



}
