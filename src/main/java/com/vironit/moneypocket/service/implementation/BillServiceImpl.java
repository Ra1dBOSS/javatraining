package com.vironit.moneypocket.service.implementation;

import com.vironit.moneypocket.dao.DAO;
import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.CreditCardValidationException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;
import com.vironit.moneypocket.service.CardValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class BillServiceImpl implements com.vironit.moneypocket.service.BillService {

    @Autowired
    @Qualifier("BillsDAO")
    private DAO<Bill> bills;

    @Autowired
    CardValidator cardValidator;

    public BillServiceImpl() {
    }

    @Override
    @Transactional
    public void addNewBill(Bill bill) throws NullException, CreditCardValidationException {
        if ((bill.getName() == null) ||
                (bill.getCardHolder() == null) ||
                (bill.getCardNumber() == null) ||
                (bill.getCvc() == null) ||
                (bill.getExpirationDate() == null) ||
                (bill.getUser() == null)) {
            throw new NullException();
        }
        if (!cardValidator.luhnCheck(bill.getCardNumber()))
            throw new CreditCardValidationException();
        bills.add(bill);
    }

    @Override
    @Transactional
    public void deleteBillById(Integer idName) throws EntityNotFoundException, NullException {
        try {
            Bill bill = bills.getById(idName);
            if (bill != null) {
                bills.delete(bill);
            } else throw new EntityNotFoundException();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public Bill findBill(Integer idName) throws EntityNotFoundException, NullException {
        Bill bill = null;
        try {
            bill = bills.getById(idName);

        } catch (IOException e) {
            e.printStackTrace();
        }
        if (bill != null)
            return bill;
        throw new EntityNotFoundException();
    }

    @Override
    @Transactional
    public List<String> getBillsNames(User user) throws NullException, EntityNotFoundException{
        if (user == null)
            throw new NullException();
        List<Bill> list = bills.getByForeignId(user.getId());
        List<String> ret = new ArrayList<>(list.size());
        for (Bill bill : list) {
            ret.add(bill.getName());
        }
        return  ret;
    }

    @Override
    @Transactional
    public List<Bill> getAllBills(User user) {
        List<Bill> list = new ArrayList<>();
        try {
            list = bills.getByForeignId(user.getId());
        } catch (NullException e) {
            e.printStackTrace();
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    @Transactional
    public Bill getBillByName(User user, String name) throws EntityNotFoundException {
        List<Bill> list = null;
        try {
            list = bills.getByForeignId(user.getId());
        } catch (NullException e) {
            e.printStackTrace();
        }
        for (Bill bill : list) {
            if (bill.getName().equals(name))
                return bill;
        }
        throw new EntityNotFoundException();
    }
}
