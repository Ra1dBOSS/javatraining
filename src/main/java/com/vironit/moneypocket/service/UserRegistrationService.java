package com.vironit.moneypocket.service;

import com.vironit.moneypocket.model.exceptions.BusyLoginException;
import com.vironit.moneypocket.model.exceptions.IncorrectPasswordException;
import com.vironit.moneypocket.model.exceptions.IncorrectPersonalInformationException;

public interface UserRegistrationService {

    public void registerNewUser(String login, String password, String name, String surname, String lastname)
            throws IncorrectPasswordException,IncorrectPersonalInformationException, BusyLoginException;

}
