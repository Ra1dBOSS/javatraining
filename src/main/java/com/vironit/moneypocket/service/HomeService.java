package com.vironit.moneypocket.service;

import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.BusyLoginException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.IncorrectPasswordException;
import com.vironit.moneypocket.model.exceptions.IncorrectPersonalInformationException;

public interface HomeService {

    public User login(String login, String password) throws EntityNotFoundException;

    public void registration(String login, String password, String name, String surname, String lastname) throws IncorrectPasswordException, BusyLoginException, IncorrectPersonalInformationException;
}
