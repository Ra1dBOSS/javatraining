package com.vironit.moneypocket.service;

public interface CardValidator {

    public boolean luhnCheck(String card);

}
