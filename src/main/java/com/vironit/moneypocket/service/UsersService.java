package com.vironit.moneypocket.service;

import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.BusyLoginException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.io.IOException;

public interface UsersService {

    public User findUser(String login, String password) throws EntityNotFoundException, NullException, IOException;

    public void add(User user) throws BusyLoginException, NullException;

    public void loginCheck(String login) throws BusyLoginException, NullException;

    public User findUserByLogin(String login) throws EntityNotFoundException;

    public void updateUserInformation(User user) throws EntityNotFoundException;

    public void deleteUser(User user) throws EntityNotFoundException;
}
