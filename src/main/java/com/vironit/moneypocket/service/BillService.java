package com.vironit.moneypocket.service;

import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.CreditCardValidationException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.beans.BeanInfo;
import java.time.LocalDateTime;
import java.util.List;

public interface BillService {

    public void addNewBill(Bill bill) throws NullException, CreditCardValidationException;

    public void deleteBillById(Integer idName) throws EntityNotFoundException, NullException;

    public Bill findBill(Integer idName) throws EntityNotFoundException, NullException;

    public List<String> getBillsNames(User user) throws NullException, EntityNotFoundException;

    public List<Bill> getAllBills(User user);

    public Bill getBillByName(User user, String name) throws EntityNotFoundException;

}
