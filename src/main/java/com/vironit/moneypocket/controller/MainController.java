package com.vironit.moneypocket.controller;


import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.UnauthorizedException;
import com.vironit.moneypocket.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class MainController {

    @Autowired
    UsersService usersService;

    @RequestMapping(value = {"/main", "/user_info"})
    public ModelAndView main(HttpSession session) throws EntityNotFoundException, UnauthorizedException{
        if (session.getAttribute("login") == null) {
            throw new UnauthorizedException();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", session.getAttribute("user"));
        return modelAndView;
    }

    @RequestMapping(value = "/fail")
    public String fail(HttpSession session) {
        return "fail";
    }
    @RequestMapping(value = "/succes")
    public String sucess(HttpSession session) {
        return "success";
    }

    @RequestMapping(value = "/logout")
    public String loguot(HttpSession session) throws UnauthorizedException{
        if (session.getAttribute("login") == null)
            throw new UnauthorizedException();
        session.removeAttribute("login");
        session.removeAttribute("user");
        return "redirect:/";
    }


    @ExceptionHandler(UnauthorizedException.class)
    public String unathorisedExceptionHandler() {
        return "unauthorized";
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public String exceptionHandler() {
        return "fail";
    }

}
