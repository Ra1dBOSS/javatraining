package com.vironit.moneypocket.controller.rest;

import com.vironit.moneypocket.dto.HomeResponseDTO;
import com.vironit.moneypocket.dto.SigninRequestDTO;
import com.vironit.moneypocket.dto.UserRequestDTO;
import com.vironit.moneypocket.dto.UserResponseDTO;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.*;
import com.vironit.moneypocket.service.HomeService;
import com.vironit.moneypocket.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
@RequestMapping(value = "/webservice")
public class HomeRestController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private HomeService homeService;

    @GetMapping(value = "/")
    @ResponseStatus(value = HttpStatus.OK)
    public HomeResponseDTO homeGet() {
        return new HomeResponseDTO();
    }

    @PostMapping(value = "/registration")
    @ResponseStatus(value = HttpStatus.CREATED)
    public HomeResponseDTO registrationPost(@RequestBody UserRequestDTO userRequestDTO, HttpSession session) throws InternalServerException, EntityNotFoundException {
        try {
            homeService.registration(userRequestDTO.getLogin(), userRequestDTO.getPassword(), userRequestDTO.getName(), userRequestDTO.getSurname(), userRequestDTO.getLastname());
        } catch (BusyLoginException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        } catch (IncorrectPasswordException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        } catch (IncorrectPersonalInformationException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        }
        return new HomeResponseDTO();
    }

    @PostMapping(value = "/signin")
    @ResponseStatus(value = HttpStatus.OK)
    public UserResponseDTO loginPost(@RequestBody SigninRequestDTO signinRequestDTO, HttpSession session) throws InternalServerException, EntityNotFoundException {
        User user = null;
        try {
            user = usersService.findUser(signinRequestDTO.getLogin(), signinRequestDTO.getPassword());
        } catch (IOException e) {
            e.printStackTrace();
            throw new InternalServerException();
        } catch (NullException e) {
            e.printStackTrace();
            throw new InternalServerException();
        }
        session.setAttribute("user", user);
        session.setAttribute("login", user.getLogin());
        return new UserResponseDTO(user);
    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public void unauthorisedExceptionHandler() {
    }

    @ExceptionHandler(InternalServerException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public void internalServerExceptionHandler() {
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void exceptionHandler() {
    }

}