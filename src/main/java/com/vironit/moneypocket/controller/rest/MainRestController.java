package com.vironit.moneypocket.controller.rest;

import com.vironit.moneypocket.dto.HomeResponseDTO;
import com.vironit.moneypocket.dto.UserRequestDTO;
import com.vironit.moneypocket.dto.UserResponseDTO;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.*;
import com.vironit.moneypocket.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
@RequestMapping(value = "/webservice/main")
public class MainRestController {

    @Autowired
    private UsersService usersService;

    @GetMapping(value = "/")
    @ResponseStatus(value = HttpStatus.OK)
    public UserResponseDTO getMain(HttpSession session) throws UnauthorizedException{
        if (session.getAttribute("login") == null)
            throw new UnauthorizedException();
        return new UserResponseDTO((User) session.getAttribute("user"));
    }

    @PutMapping(value = "/updateuser")
    @ResponseStatus(value = HttpStatus.OK)
    public UserResponseDTO updateUser(@RequestBody UserRequestDTO userRequestDTO, HttpSession session)
            throws EntityNotFoundException, AccessDeniedException, UnauthorizedException{
        User user = null;
        if (session.getAttribute("login") == null)
            throw new UnauthorizedException();
        if (!((User)session.getAttribute("user")).getId().equals(userRequestDTO.getId()))
            throw new AccessDeniedException();
        usersService.updateUserInformation(userRequestDTO.toUser());
        user = userRequestDTO.toUser();
        session.removeAttribute("login");
        session.removeAttribute("user");
        session.setAttribute("login", user.getLogin());
        session.setAttribute("user", user);
        return new UserResponseDTO(userRequestDTO);
    }

    @DeleteMapping(value = "/deleteuser")
    @ResponseStatus(value = HttpStatus.OK)
    public HomeResponseDTO deleteUser(@RequestBody UserRequestDTO userRequestDTO, HttpSession session)
            throws EntityNotFoundException, AccessDeniedException, UnauthorizedException {
        if (session.getAttribute("login") == null)
            throw new UnauthorizedException();
        if (!((User)session.getAttribute("user")).getId().equals(userRequestDTO.getId()))
            throw new AccessDeniedException();
        usersService.deleteUser(userRequestDTO.toUser());
        session.removeAttribute("login");
        session.removeAttribute("user");
        return new HomeResponseDTO();
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public void accessDenied() {

    }

    @ExceptionHandler(UnauthorizedException.class)
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public void unauthorisedExceptionHandler() {
    }

    @ExceptionHandler(InternalServerException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public void internalServerExceptionHandler() {
    }

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public void exceptionHandler() {
    }

}
