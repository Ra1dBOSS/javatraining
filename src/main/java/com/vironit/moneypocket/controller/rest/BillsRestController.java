package com.vironit.moneypocket.controller.rest;

import com.vironit.moneypocket.dto.BillResponseDTO;
import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.AccessDeniedException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.InternalServerException;
import com.vironit.moneypocket.model.exceptions.UnauthorizedException;
import com.vironit.moneypocket.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/webservice/main/bills")
public class BillsRestController {

    @Autowired
    BillService billService;

    @GetMapping(value = "/")
    @ResponseStatus(value = HttpStatus.OK)
    public List<BillResponseDTO> getBills(HttpSession session) throws UnauthorizedException{
        if (session.getAttribute("login") == null)
            throw new UnauthorizedException();
        List<Bill> list = billService.getAllBills((User) session.getAttribute("user"));
        List<BillResponseDTO> list2= new ArrayList<>(list.size());
        for (Bill x : list)
            list2.add(new BillResponseDTO(x));
        return list2;
    }

}
