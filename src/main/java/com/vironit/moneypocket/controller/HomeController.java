package com.vironit.moneypocket.controller;

import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
public class HomeController {

    @Autowired
    HomeService homeService;

    @RequestMapping("/")
    public String welcome(Model model, HttpSession session) {
        if (session.getAttribute("login") != null)
            return "redirect:/main";
        return "home";
    }

    @RequestMapping("/login_fail")
    public String loginFail(Model model) {
        return "login_fail";
    }

    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String signinGetPage(Model model) {
        return "signin";
    }


    @RequestMapping(value = "/signin", method = RequestMethod.POST)
    public ModelAndView signin(@ModelAttribute("registrationForm")User user, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/main");
        try {
            user = homeService.login(user.getLogin(), user.getPassword());
        } catch (EntityNotFoundException e) {
            modelAndView.setViewName("redirect:/login_fail");
            return modelAndView;
        }
        session.setAttribute("login", user.getLogin());
        session.setAttribute("user", user);
        return modelAndView;
    }

}
