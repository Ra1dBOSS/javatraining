package com.vironit.moneypocket.controller;

import com.vironit.moneypocket.dto.BillAddDTO;
import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.CreditCardValidationException;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;
import com.vironit.moneypocket.model.exceptions.UnauthorizedException;
import com.vironit.moneypocket.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

@Controller
public class BillsController {

    @Autowired
    BillService billService;

    @RequestMapping("/bills")
    public ModelAndView bills(HttpSession session) throws EntityNotFoundException, UnauthorizedException{
        ModelAndView modelAndView = new ModelAndView("bills");
        if (session.getAttribute("login") == null) {
            throw new UnauthorizedException();
        }
        User user = (User) session.getAttribute("user");
        modelAndView.addObject("user",user);
        modelAndView.addObject("bills", billService.getAllBills(user));
        return modelAndView;
    }

    @RequestMapping(value = "/bill_info", method = RequestMethod.GET)
    public ModelAndView billsInfo(@ModelAttribute("name") String name, HttpSession session) throws EntityNotFoundException, UnauthorizedException{
        ModelAndView modelAndView = new ModelAndView("bill_info");
        if (session.getAttribute("login") == null) {
            throw new UnauthorizedException();
        }
        User user = (User) session.getAttribute("user");
        modelAndView.addObject("user",user);
        modelAndView.addObject("bill", billService.getBillByName(user, name));
        return modelAndView;
    }

    @RequestMapping(value = "/bill_info/delete", method = RequestMethod.GET)
    public String billDelete(@ModelAttribute("bill.id") Integer id, HttpSession session) throws UnauthorizedException, EntityNotFoundException{
        if (session.getAttribute("login") == null) {
            throw new UnauthorizedException();
        }
        User user = (User) session.getAttribute("user");
        try {
            billService.deleteBillById(id);
        } catch (NullException e) {
            e.printStackTrace();
            throw new EntityNotFoundException();
        }
        return "redirect:/success";
    }

    @RequestMapping(value = "/bill_binding", method = RequestMethod.GET)
    public String billAddGetPage(HttpSession session) {
        if (session.getAttribute("login") == null) {
            throw new RuntimeException();
        }
        return "bill_binding";
    }

    @RequestMapping(value = "/bill_binding", method = RequestMethod.POST)
    public ModelAndView billAdd(@ModelAttribute("billForm") BillAddDTO billAddDTO, HttpSession session) throws UnauthorizedException {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:/success");
        if (session.getAttribute("login") == null) {
            throw new UnauthorizedException();
        }
        try {
            Bill bill = billAddDTO.getBill();
            bill.setUser((User)session.getAttribute("user"));
            billService.addNewBill(bill);
        } catch (NullException e) {
            modelAndView.setViewName("redirect:/fail");
            return modelAndView;
        } catch (CreditCardValidationException e) {
            modelAndView.setViewName("redirect:/fail");
            return modelAndView;
        }
        return modelAndView;
    }

}
