package com.vironit.moneypocket.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Table (name = "transactions")
public class Transaction {

    @Column(name = "date")
    private LocalDateTime date;

    @Column(name = "amount")
    private double amount;

    @Column(name = "comment")
    private String comment;

    @Column(name = "id")
    private int id;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "bill_id")
    private Bill bill;

    public Transaction(double amount, String comment, int id, LocalDateTime date) {
        this( amount, comment, id);
        this.date = date;
    }

    public Transaction(double amount, String comment, int id) {
        this.date = LocalDateTime.now();
        this.amount = amount;
        this.comment = comment;
        this.id = id;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public String getComment() {
        return comment;
    }

    public Integer getId() {
        return id;
    }

    public Bill getBill() {
        return bill;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Transaction)) return false;
        Transaction that = (Transaction) o;
        return Double.compare(that.getAmount(), getAmount()) == 0 &&
                getId() == that.getId() &&
                Objects.equals(getDate(), that.getDate()) &&
                Objects.equals(getComment(), that.getComment());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getDate(), getAmount(), getComment(), getId());
    }
}
