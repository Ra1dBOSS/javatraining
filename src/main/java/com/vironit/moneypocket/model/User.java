package com.vironit.moneypocket.model;

import com.vironit.moneypocket.model.exceptions.IncorrectPersonalInformationException;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "users", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column (name = "id")
    private Integer id;

    @Column (name = "login")
    private String login;

    @Column (name = "password")
    private String password;

    @Column (name = "name")
    private String name;

    @Column (name = "surname")
    private String surname;

    @Column (name = "lastname")
    private String lastname;

    public User() {
    }

    public User(Integer id, String login, String password, String name, String surname, String lastname) throws IncorrectPersonalInformationException {
        this(login, password, name, surname, lastname);
        this.id = id;
    }

    public User(String login, String password, String name, String surname, String lastname) throws IncorrectPersonalInformationException {
        this.login = login;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.lastname = lastname;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return Objects.equals(getId(), user.getId()) &&
                Objects.equals(getLogin(), user.getLogin()) &&
                Objects.equals(getPassword(), user.getPassword()) &&
                Objects.equals(getName(), user.getName()) &&
                Objects.equals(getSurname(), user.getSurname()) &&
                Objects.equals(getLastname(), user.getLastname());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getLogin(), getPassword(), getName(), getSurname(), getLastname());
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setId(Integer idName) {
        this.id = idName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
