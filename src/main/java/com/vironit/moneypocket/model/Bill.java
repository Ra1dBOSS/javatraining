package com.vironit.moneypocket.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "bills", schema = "public", catalog = "MoneyPocketDB")
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "card_holder")
    private String cardHolder;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "card_expiration_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime expirationDate;

    @Column(name = "cvc")
    private Integer cvc;

    @Column(name = "balance")
    private double balance;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    public Bill() {
    }

    public Bill(Integer id, String name, String cardHolder, String cardNumber, LocalDateTime expirationDate, Integer cvc, double balance) {

        this.id = id;
        this.name = name;
        this.cardHolder = cardHolder;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cvc = cvc;
        this.balance = balance;
    }

    public Bill(String name, String cardHolder, String cardNumber, LocalDateTime expirationDate, Integer cvc, double balance) {
        this.name = name;
        this.cardHolder = cardHolder;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cvc = cvc;
        this.balance = balance;
    }

    public Bill(User user, String name, String cardHolder, String cardNumber, LocalDateTime expirationDate, Integer cvc, double balance) {
        this.user = user;
        this.name = name;
        this.cardHolder = cardHolder;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
        this.cvc = cvc;
        this.balance = balance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getCvc() {
        return cvc;
    }

    public void setCvc(Integer cvc) {
        this.cvc = cvc;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bill)) return false;
        Bill bill = (Bill) o;
        return Objects.equals(getId(), bill.getId()) &&
                Objects.equals(getName(), bill.getName()) &&
                Objects.equals(getCardHolder(), bill.getCardHolder()) &&
                Objects.equals(getCardNumber(), bill.getCardNumber()) &&
                Objects.equals(getExpirationDate(), bill.getExpirationDate()) &&
                Objects.equals(getCvc(), bill.getCvc());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getCardHolder(), getCardNumber(), getExpirationDate(), getCvc());
    }
}
