package com.vironit.moneypocket.dao;

import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.io.IOException;

public interface UsersDAO extends DAO<User> {
    public User getByLogin(String login) throws IOException, NullException, EntityNotFoundException;
}
