package com.vironit.moneypocket.dao.file;

import com.google.gson.Gson;
import com.vironit.moneypocket.dao.DAO;
import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.Transaction;
import com.vironit.moneypocket.model.exceptions.NullException;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class BillsFileDAO implements DAO<Bill> {

    String path;

    public BillsFileDAO() {
        this("bills.txt");
    }

    public BillsFileDAO(String path) {
        this.path = path;
        if (!Files.exists(Paths.get(path))) {
            try {
                Files.createFile(Paths.get(path));
            } catch (IOException e) {

            }
        }
    }

    @Override
    public void add(Bill bill) {
        Gson gson = new Gson();
        String json = gson.toJson(bill) + "\n";
        try {
            Files.write(Paths.get(path), json.getBytes(), StandardOpenOption.APPEND);
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    @Override
    public List<Bill> getByForeignId(Integer foreignId) throws NullException {
        if (foreignId == null) {
            throw new NullException();
        }
        List<Bill> t = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            Gson gson = new Gson();
            for (String line : lines) {
                Bill bill = gson.fromJson(line, Bill.class);
                if (foreignId.equals(bill.getUser().getId())) {
                    t.add(bill);
                }
            }
        } catch (IOException e) { e.printStackTrace();}
        return t;
    }

    @Override
    public Bill getById(Integer id) throws IOException, NullException, EntityNotFoundException {
        if (id == null) {
            throw new NullException();
        }
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Gson gson = new Gson();
        for(String line: lines){
            Bill bill = gson.fromJson(line, Bill.class);
            if (bill.getId().equals(bill.getId())) {
                return bill;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void update(Bill bill) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Files.delete(Paths.get(path));
        Files.createFile(Paths.get(path));
        Gson gson = new Gson();
        for (String line : lines) {
            Bill billFromJson = gson.fromJson(line, Bill.class);
            if (bill.getId() != billFromJson.getId()) {
                this.add(billFromJson);
            } else {
                this.add(bill);
            }
        }
    }

    @Override
    public void delete(Bill bill) throws IOException{
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Files.delete(Paths.get(path));
        Files.createFile(Paths.get(path));
        Gson gson = new Gson();
        for (String line : lines) {
            Bill billFromJson = gson.fromJson(line, Bill.class);
            if (!bill.equals(billFromJson)) {
                this.add(billFromJson);
            }
        }
    }
}
