package com.vironit.moneypocket.dao.file;

import com.google.gson.Gson;
import com.vironit.moneypocket.dao.UsersDAO;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class UserFileDAO implements UsersDAO {


    String path;

    public UserFileDAO() {
        this("users.txt");
    }

    public UserFileDAO(String path) {
        this.path = path;
        if (!Files.exists(Paths.get(path))) {
            try {
                Files.createFile(Paths.get(path));
            } catch (IOException e) {

            }
        }
    }

    @Override
    public List<User> getByForeignId(Integer foreignId) throws NullException {
        return null;
    }

    @Override
    public void add(User user) {
        Gson gson = new Gson();
        String json = gson.toJson(user) + "\n";
        try {
            Files.write(Paths.get(path), json.getBytes(), StandardOpenOption.APPEND);
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    @Override
    public User getById(Integer idName) throws IOException, NullException, EntityNotFoundException{
        if (idName == null) {
            throw new NullException();
        }
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Gson gson = new Gson();
        for(String line: lines){
            User user = gson.fromJson(line, User.class);
            if (user.getId().equals(idName)) {
                return user;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public User getByLogin(String login) throws IOException, NullException, EntityNotFoundException {
        if (login == null) {
            throw new NullException();
        }
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Gson gson = new Gson();
        for(String line: lines){
            User user = gson.fromJson(line, User.class);
            if (user.getLogin().equals(login)) {
                return user;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void update(User user) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Files.delete(Paths.get(path));
        Files.createFile(Paths.get(path));
        Gson gson = new Gson();
        for (String line : lines) {
            User userFromJson = gson.fromJson(line, User.class);
            if (user.getId() != userFromJson.getId()) {
                this.add(userFromJson);
            } else {
                this.add(user);
            }
        }
    }

    @Override
    public void delete(User user) throws IOException{
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Files.delete(Paths.get(path));
        Files.createFile(Paths.get(path));
        Gson gson = new Gson();
        for (String line : lines) {
            User userFromJson = gson.fromJson(line, User.class);
            if (!user.equals(userFromJson)) {
                this.add(userFromJson);
            }
        }
    }
}
