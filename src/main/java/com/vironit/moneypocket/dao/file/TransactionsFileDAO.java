package com.vironit.moneypocket.dao.file;

import com.google.gson.Gson;
import com.vironit.moneypocket.dao.DAO;
import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.Transaction;
import com.vironit.moneypocket.model.exceptions.NullException;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class TransactionsFileDAO implements DAO<Transaction> {

    String path;

    public TransactionsFileDAO() {
        this("transactions.txt");
    }

    public TransactionsFileDAO(String path) {
        this.path = path;
        if (!Files.exists(Paths.get(path))) {
            try {
                Files.createFile(Paths.get(path));
            } catch (IOException e) {

            }
        }
    }

    @Override
    public void add(Transaction transaction) {
        Gson gson = new Gson();
        String json = gson.toJson(transaction) + "\n";
        try {
            Files.write(Paths.get(path), json.getBytes(), StandardOpenOption.APPEND);
        }
        catch (IOException e) {
            System.out.println(e);
        }
    }

    @Override
    public List<Transaction> getByForeignId(Integer foreignId) throws NullException {
        if (foreignId == null) {
            throw new NullException();
        }
        List<Transaction> t = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            Gson gson = new Gson();
            for (String line : lines) {
                Transaction transaction = gson.fromJson(line, Transaction.class);
                if (foreignId.equals(transaction.getBill())) {
                    t.add(transaction);
                }
            }

        } catch (IOException e) { e.printStackTrace();}
        return t;
    }

    @Override
    public Transaction getById(Integer idName) throws IOException, NullException, EntityNotFoundException {
        if (idName == null) {
            throw new NullException();
        }
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Gson gson = new Gson();
        for(String line: lines){
            Transaction transaction = gson.fromJson(line, Transaction.class);
            if (transaction.getId().equals(transaction.getId())) {
                return transaction;
            }
        }
        throw new EntityNotFoundException();
    }

    @Override
    public void update(Transaction transaction) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Files.delete(Paths.get(path));
        Files.createFile(Paths.get(path));
        Gson gson = new Gson();
        for (String line : lines) {
            Transaction transactionFromJson = gson.fromJson(line, Transaction.class);
            if (transaction.getId() != transactionFromJson.getId()) {
                this.add(transactionFromJson);
            } else {
                this.add(transaction);
            }
        }
    }

    @Override
    public void delete(Transaction transaction) throws IOException{
        List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
        Files.delete(Paths.get(path));
        Files.createFile(Paths.get(path));
        Gson gson = new Gson();
        for (String line : lines) {
            Transaction transactionFromJson = gson.fromJson(line, Transaction.class);
            if (!transaction.equals(transactionFromJson)) {
                this.add(transactionFromJson);
            }
        }
    }
}
