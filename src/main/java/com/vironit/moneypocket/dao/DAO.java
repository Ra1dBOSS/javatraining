package com.vironit.moneypocket.dao;

import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.io.IOException;
import java.util.List;

public interface DAO<T> {

    public void add(T t);

    public T getById(Integer idName) throws IOException, NullException, EntityNotFoundException;

    public List<T> getByForeignId(Integer foreignId) throws NullException, EntityNotFoundException;

    public void update(T t) throws IOException, EntityNotFoundException;

    public void delete(T t) throws IOException, EntityNotFoundException;

}
