package com.vironit.moneypocket.dao.jdbs;

import com.vironit.moneypocket.dao.UsersDAO;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.IncorrectPersonalInformationException;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsersDBDAO implements UsersDAO {

    Connection conn;

    public UsersDBDAO(){

    }

    private void makeCon() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/MoneyPocketDB", "postgres", "postgres");
        } catch (Exception e) {e.printStackTrace();}
    }

    @Override
    public List<User> getByForeignId(Integer foreignId) throws NullException {
        return null;
    }

    @Override
    public void add(User user) {
        makeCon();
        String insertSql = "INSERT INTO users (login, password, name, surname, lastname) VALUES (?, ?, ?, ?, ?);";
        try {
            PreparedStatement statement = conn.prepareStatement(insertSql);
            statement.setString(1,user.getLogin());
            statement.setString(2,user.getPassword());
            statement.setString(3,user.getName());
            statement.setString(4,user.getSurname());
            statement.setString(5,user.getLastname());
            statement.executeUpdate();
            statement.close();
            conn.close();
        } catch (SQLException e) {e.printStackTrace();}
    }

    @Override
    public User getById(Integer idName) throws IOException, NullException {
        if (idName == null)
            throw new NullException();
        List<User> t = new ArrayList<>();
        String sql = "SELECT * FROM users WHERE id=" + idName + ";";
        makeCon();
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String login = rs.getString("login");
                idName = rs.getInt("id");
                String password = rs.getString("password");
                String name = rs.getString("login");
                String surname = rs.getString("surname");
                String lastname = rs.getString("lastname");
                User newUser = new User(idName, login, password, name, surname, lastname);
                t.add(newUser);
            }
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IncorrectPersonalInformationException e) { }
        return t.size() > 0 ? t.get(0) : null;
    }

    @Override
    public User getByLogin(String login) throws IOException, NullException, EntityNotFoundException {
        makeCon();
        if (login == null)
            throw new NullException();
        List<User> t = new ArrayList<>();
        String sql = "SELECT * FROM users WHERE login='" + login + "';";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                login = rs.getString("login");
                Integer idName = rs.getInt("id");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String lastname = rs.getString("lastname");
                User newUser = new User(idName, login, password, name, surname, lastname);
                t.add(newUser);
            }
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IncorrectPersonalInformationException e) {
            e.printStackTrace();
        }
        if (t.size() > 0)
            return t.get(0);
        throw new EntityNotFoundException();
    }

    @Override
    public void update(User t) throws IOException {
        makeCon();
        String sql = "UPDATE users SET login=?, password=?, name=?, surname=?, lastname=? WHERE id=?;";
        try {
            PreparedStatement statement;
            statement = conn.prepareStatement(sql);
            User x = t;
            statement.setString(1,x.getLogin());
            statement.setString(2,x.getPassword());
            statement.setString(3,x.getName());
            statement.setString(4,x.getSurname());
            statement.setString(5,x.getLastname());
            statement.setInt(6, x.getId());statement.executeQuery();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(User user) throws IOException {
        makeCon();
        String sql = "DELETE users WHERE id=?;";
        try {
            PreparedStatement statement;
            statement = conn.prepareStatement(sql);
            statement.setInt(1,user.getId());
            statement.executeQuery();
            statement.close();
            conn.close();
        } catch (SQLException e) {e.printStackTrace();}
    }

}
