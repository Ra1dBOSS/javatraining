package com.vironit.moneypocket.dao.jdbs;

import com.vironit.moneypocket.dao.DAO;
import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.exceptions.NullException;

import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BillsDBDAO implements DAO<Bill> {

    Connection conn;

    public BillsDBDAO() {

    }

    private void makeCon() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/MoneyPocketDB", "postgres", "postgres");
        } catch (Exception e) {e.printStackTrace();}
    }


    @Override
    public void add(Bill bill) {
        makeCon();
        String insertSql = "INSERT INTO bills (user_id, balance, card_number, card_holder, card_expiration_date, cvc, name) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";
        try {
            PreparedStatement statement = conn.prepareStatement(insertSql);
            statement.setInt(1,bill.getUser().getId());
            statement.setDouble(2,bill.getBalance());
            statement.setString(3,bill.getCardNumber());
            statement.setString(4,bill.getCardHolder());
            statement.setObject(5,bill.getExpirationDate());
            statement.setInt(6,bill.getCvc());
            statement.setString(7,bill.getName());
            statement.executeUpdate();
            statement.close();
            conn.close();
        } catch (SQLException e) {e.printStackTrace();}
    }

    @Override
    public Bill getById(Integer idName) throws NullException {
        makeCon();
        if (idName == null)
            throw new NullException();
        List<Bill> t = new ArrayList<>();
        String sql = "SELECT * FROM bills WHERE id=" + idName + ";";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                idName = rs.getInt("id");
                Integer user_id = rs.getInt("user_id");
                Double balance = rs.getDouble("balance");
                String card_number = rs.getString("card_number");
                String card_holder = rs.getString("card_holder");
                LocalDateTime card_expiration_date = rs.getObject("card_expiration_date", LocalDateTime.class);
                Integer cvc = rs.getInt("cvc");
                String name = rs.getString("name");
                Bill newBill = new Bill(idName, name, card_number, card_holder, card_expiration_date, cvc, balance);
                t.add(newBill);
            }
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (t.size() > 0)
            return t.get(0);
        throw new EntityNotFoundException();
    }

    @Override
    public List<Bill> getByForeignId(Integer foreignId) throws NullException {
        makeCon();
        if (foreignId == null)
            throw new NullException();
        List<Bill> t = new ArrayList<>();
        String sql = "SELECT * FROM bills WHERE user_id=" + foreignId + ";";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Integer idName = rs.getInt("id");
                Integer user_id = rs.getInt("user_id");
                Double balance = rs.getDouble("balance");
                String card_number = rs.getString("card_number");
                String card_holder = rs.getString("card_holder");
                LocalDateTime card_expiration_date = rs.getObject("card_expiration_date", LocalDateTime.class);
                Integer cvc = rs.getInt("cvc");
                String name = rs.getString("name");
                Bill newBill = new Bill(idName, name, card_number, card_holder, card_expiration_date, cvc, balance);
                t.add(newBill);
            }
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return t;
    }

    @Override
    public void update(Bill t) throws IOException {
        makeCon();
        String sql = "UPDATE bills SET user_id=?, balance=?, card_number=?, card_holder=?, card_expiration_date=?, cvc=?, name=? WHERE id=?;";
        try {
            PreparedStatement statement;
            statement = conn.prepareStatement(sql);
            Bill x = t;
            statement.setInt(1,x.getUser().getId());
            statement.setDouble(2,x.getBalance());
            statement.setString(3,x.getCardNumber());
            statement.setString(4,x.getCardHolder());
            statement.setObject(5,x.getExpirationDate());
            statement.setInt(6, x.getCvc());
            statement.setString(7, x.getName());
            statement.setInt(8, x.getId());
            statement.executeQuery();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Bill bill) throws IOException {
        makeCon();
        String sql = "DELETE bills WHERE id=?;";
        try {
            PreparedStatement statement;
            statement = conn.prepareStatement(sql);
            statement.setInt(1,bill.getId());
            statement.executeQuery();
            statement.close();
            conn.close();
        } catch (SQLException e) {e.printStackTrace();}
    }
}
