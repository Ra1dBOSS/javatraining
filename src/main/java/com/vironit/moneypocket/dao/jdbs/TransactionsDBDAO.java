package com.vironit.moneypocket.dao.jdbs;

import com.vironit.moneypocket.dao.DAO;
import com.vironit.moneypocket.model.Transaction;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class TransactionsDBDAO implements DAO<Transaction> {

    Connection conn;

    public TransactionsDBDAO(){
    }

    private void makeCon() {
        try {
            Class.forName("org.postgresql.Driver");
            conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/MoneyPocketDB", "postgres", "postgres");
        } catch (Exception e) {e.printStackTrace();}
    }


    @Override
    public void add(Transaction transaction) {
        makeCon();
        String insertSql = "INSERT INTO transactions (bill_id, date, amount, comment) VALUES (";
        insertSql += transaction.getBill().getId()+ ", ";
        insertSql += "?"+ ", ";
        insertSql += transaction.getAmount()+ ", ";
        insertSql += "'" + transaction.getComment()+ "');";
        try {
            PreparedStatement statement = conn.prepareStatement(insertSql);
            statement.setObject(1, transaction.getDate());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {e.printStackTrace();}
    }

    @Override
    public Transaction getById(Integer idName) throws IOException, NullException, EntityNotFoundException {
        makeCon();
        if (idName == null)
            throw new NullException();
        List<Transaction> t = new ArrayList<>();
        String sql = "SELECT * FROM transactions WHERE id=" + idName + ";";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Double amount = rs.getDouble("amount");
                String comment = rs.getString("comment");
                LocalDateTime date = rs.getObject("date",LocalDateTime.class);
                Integer billIdName = rs.getInt("bill_id");
                idName = rs.getInt("id");
                Transaction newTransaction = new Transaction(amount, comment, idName, date);
                t.add(newTransaction);
            }
            statement.close();
        } catch (SQLException e) {e.printStackTrace();}
        if (t.size() > 0) {
            return t.get(0);
        }
        throw new EntityNotFoundException();
    }

    @Override
    public List<Transaction> getByForeignId(Integer foreignId) throws NullException {
        makeCon();
        if (foreignId == null)
            throw new NullException();
        List<Transaction> t = new ArrayList<>();
        String sql = "SELECT * FROM transactions WHERE bill_id=" + foreignId + ";";
        try {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Double amount = rs.getDouble("amount");
                String comment = rs.getString("comment");
                LocalDateTime date = rs.getObject("date",LocalDateTime.class);
                Integer billIdName = rs.getInt("bill_id");
                Integer idName = rs.getInt("id");
                Transaction newTransaction = new Transaction(amount, comment, idName, date);
                t.add(newTransaction);
            }
            statement.close();
        } catch (SQLException e) {e.printStackTrace();}
        return t;
    }

    @Override
    public void update(Transaction t) throws IOException {
        makeCon();
        String sql = "UPDATE transactions SET bill_id=?, date=?, amount=?, comment=? WHERE id=?;";
        try {
            PreparedStatement statement;
            statement = conn.prepareStatement(sql);
            Transaction x = t;
            statement.setInt(1,x.getBill().getId());
            statement.setObject(2,x.getDate());
            statement.setDouble(3,x.getAmount());
            statement.setString(4,x.getComment());
            statement.setInt(5,x.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {e.printStackTrace();}
    }

    @Override
    public void delete(Transaction transaction) throws IOException {
        makeCon();
        String sql = "DELETE FROM transactions WHERE id=?;";
        try {
            PreparedStatement statement;
            statement = conn.prepareStatement(sql);
            statement.setInt(1,transaction.getId());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e) {e.printStackTrace();}
    }
}
