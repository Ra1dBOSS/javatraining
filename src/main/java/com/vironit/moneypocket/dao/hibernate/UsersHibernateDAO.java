package com.vironit.moneypocket.dao.hibernate;

import com.vironit.moneypocket.dao.UsersDAO;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.NullException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.List;

@Repository("UsersDAO")
public class UsersHibernateDAO extends AbstractHibernateDAO<User> implements UsersDAO {

    @Override
    public User getByLogin(String login) throws IOException, NullException {
        List<User> ret = null;
        org.hibernate.Session session = sessionFactory.getCurrentSession();
        EntityManager entityManager = session.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> root = criteriaQuery.from(User.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("login"), login));
        ret = entityManager.createQuery(criteriaQuery).getResultList();
        return ret.size() > 0 ? ret.get(0) : null;
    }

    @Override
    public List<User> getByForeignId(Integer foreignId) throws NullException {
        return null;
    }
}
