package com.vironit.moneypocket.dao.hibernate;

import com.vironit.moneypocket.dao.DAO;
import com.vironit.moneypocket.dao.hibernate.HibernateUtil;
import com.vironit.moneypocket.model.exceptions.NullException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import javax.transaction.Transactional;
import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.util.List;


public abstract class AbstractHibernateDAO<T> implements DAO<T> {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void add(T t) {
        org.hibernate.Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(t);
    }

    @Override
    public T getById(Integer idName) throws IOException, NullException {
        List<T> ret = null;
        org.hibernate.Session session = sessionFactory.getCurrentSession();
        EntityManager entityManager = session.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery((Class<T>)
                ((ParameterizedType)getClass()
                        .getGenericSuperclass())
                        .getActualTypeArguments()[0]);
        Root<T> root = criteriaQuery.from((Class<T>)
                ((ParameterizedType)getClass()
                        .getGenericSuperclass())
                        .getActualTypeArguments()[0]);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("id"), idName));
        ret = entityManager.createQuery(criteriaQuery).getResultList();

        return ret.size() > 0 ? ret.get(0) : null;
    }

    @Override
    public void update(T t) throws IOException {
        org.hibernate.Session session = sessionFactory.getCurrentSession();
        session.update(t);
    }

    @Override
    public void delete(T t) throws IOException {
        org.hibernate.Session session = sessionFactory.getCurrentSession();
        session.delete(t);
    }
}
