package com.vironit.moneypocket.dao.hibernate;

import com.vironit.moneypocket.model.Transaction;
import com.vironit.moneypocket.model.exceptions.NullException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository("TransactionsDAO")
public class TransactionsHibernateDAO extends AbstractHibernateDAO {

    @Override
    public List getByForeignId(Integer foreignId) throws NullException {
        List<Transaction> ret = null;
        org.hibernate.Session session = sessionFactory.getCurrentSession();
        EntityManager entityManager = session.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Transaction> criteriaQuery = criteriaBuilder.createQuery(Transaction.class);
        Root<Transaction> root = criteriaQuery.from(Transaction.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("bill"), foreignId));
        ret = entityManager.createQuery(criteriaQuery).getResultList();

        return ret;
    }
}
