package com.vironit.moneypocket.dao.hibernate;

import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.exceptions.NullException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository("BillsDAO")
public class BillsHibernateDAO extends AbstractHibernateDAO<Bill> {

    @Override
    public List<Bill> getByForeignId(Integer foreignId) throws NullException {
        List<Bill> ret = null;
        org.hibernate.Session session = sessionFactory.getCurrentSession();
        EntityManager entityManager = session.getEntityManagerFactory().createEntityManager();
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Bill> criteriaQuery = criteriaBuilder.createQuery(Bill.class);
        Root<Bill> root = criteriaQuery.from(Bill.class);
        criteriaQuery.select(root);
        criteriaQuery.where(criteriaBuilder.equal(root.get("user"), foreignId));
        ret = entityManager.createQuery(criteriaQuery).getResultList();
        return ret;
    }
}
