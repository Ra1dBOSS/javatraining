package com.vironit.moneypocket.configuration;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.vironit.moneypocket")
public class RootApplicationContextConfig {



}
