package com.vironit.moneypocket.dto;

import com.vironit.moneypocket.model.User;

public class UserRequestDTO {

    private Integer id;

    private String login;
    private String password;

    private String name;
    private String surname;
    private String lastname;

    public UserRequestDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.lastname = user.getLastname();
    }

    public UserRequestDTO(UserResponseDTO userResponseDTO) {
        this.id = userResponseDTO.getId();
        this.login = userResponseDTO.getLogin();
        this.password = userResponseDTO.getPassword();
        this.name = userResponseDTO.getName();
        this.surname = userResponseDTO.getSurname();
        this.lastname = userResponseDTO.getLastname();
    }

    public User toUser() {
        User user = new User();
        user.setSurname(surname);
        user.setLogin(login);
        user.setLastname(lastname);
        user.setName(name);
        user.setId(id);
        user.setPassword(password);
        return user;
    }

    public UserRequestDTO() {

    }

    public SigninRequestDTO toSigninRequestDTO() {
        SigninRequestDTO ret = new SigninRequestDTO();
        ret.setLogin(login);
        ret.setPassword(password);
        return ret;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLastname() {
        return lastname;
    }
}
