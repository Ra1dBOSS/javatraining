package com.vironit.moneypocket.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vironit.moneypocket.model.User;

public class UserResponseDTO {

    private Integer id;

    private String login;
    private String password;

    private String name;
    private String surname;
    private String lastname;

    @JsonProperty(value = "get_bills")
    final private String getBillsUri = "/webservice/main/bills";
    @JsonProperty(value = "update_user_information")
    final private String updateUserUri = "/webservice/main/updateuser";

    public UserResponseDTO(User user) {
        this.id = user.getId();
        this.login = user.getLogin();
        this.password = user.getPassword();
        this.name = user.getName();
        this.surname = user.getSurname();
        this.lastname = user.getLastname();
    }

    public UserResponseDTO() {

    }

    public User toUser() {
        User user = new User();
        user.setSurname(surname);
        user.setLogin(login);
        user.setLastname(lastname);
        user.setName(name);
        user.setId(id);
        user.setPassword(password);
        return user;
    }

    public UserResponseDTO(UserRequestDTO userRequestDTO) {
        this.id = userRequestDTO.getId();
        this.login = userRequestDTO.getLogin();
        this.password = userRequestDTO.getPassword();
        this.name = userRequestDTO.getName();
        this.surname = userRequestDTO.getSurname();
        this.lastname = userRequestDTO.getLastname();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getId() {
        return id;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getGetBillsUri() {
        return getBillsUri;
    }

    public String getUpdateUserUri() {
        return updateUserUri;
    }
}
