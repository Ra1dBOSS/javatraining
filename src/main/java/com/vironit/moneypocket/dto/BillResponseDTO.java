package com.vironit.moneypocket.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vironit.moneypocket.model.Bill;

import java.time.LocalDateTime;

public class BillResponseDTO {

    private int id;

    @JsonProperty(value = "card_number")
    private String cardNumber;
    @JsonProperty(value = "card_holder")
    private String cardHolder;

    @JsonProperty(value = "card_expiration_date")
    private LocalDateTime expirationDate;

    private int cvc;

    private String name;

    @JsonProperty(value = "delete_this_bill")
    private String deleteBill = "/webservice/main/bills/delete?id=" + id;
    @JsonProperty(value = "get_all_transactions")
    private String getTransactions = "/webservice/main/bills/transactions?id=" + id;


    public BillResponseDTO() {

    }

    public BillResponseDTO(Bill bill) {
        this.setId(bill.getId());
        this.cardNumber = bill.getCardNumber();
        this.cardHolder = bill.getCardHolder();
        this.expirationDate = bill.getExpirationDate();
        this.cvc = bill.getCvc();
        this.name = bill.getName();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        deleteBill = "/webservice/main/bills/delete?id=" + id;
        getTransactions = "/webservice/main/bills/transactions?id=" + id;
        this.id = id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public LocalDateTime getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(LocalDateTime expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getCvc() {
        return cvc;
    }

    public void setCvc(int cvc) {
        this.cvc = cvc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeleteBill() {
        return deleteBill;
    }

    public String getGetTransactions() {
        return getTransactions;
    }
}
