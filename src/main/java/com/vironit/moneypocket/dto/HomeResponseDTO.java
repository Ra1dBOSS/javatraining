package com.vironit.moneypocket.dto;

public class HomeResponseDTO {

    final private String registrationUri = "/webservice/registration";
    final private String signinUri = "/webservice/signin";

    public String getRegistrationUri() {
        return registrationUri;
    }

    public String getSigninUri() {
        return signinUri;
    }
}
