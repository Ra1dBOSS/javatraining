package com.vironit.moneypocket.dto;

import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.NullException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

public class BillAddDTO {

    private String name;
    private String cardNumber;
    private String cardHolder;
    private String expirationDate;
    private String cvc;
    private String balance;

    public Bill getBill() throws NullException {
        if ((name == null) || (name.length() == 0)) {
            throw new NullException();
        }
        if ((cardHolder == null) || (cardHolder.length() == 0)) {
            throw new NullException();
        }
        if ((cardNumber == null) || (cardNumber.length() == 0)) {
            throw new NullException();
        }
        if ((expirationDate == null) || (expirationDate.length() == 0)) {
            throw new NullException();
        }
        if ((cvc == null) || (cvc.length() == 0)) {
            throw new NullException();
        }
        if ((balance == null) || (balance.length() == 0)) {
            throw new NullException();
        }
        LocalDateTime ldt = null;
        try {
            ldt = LocalDateTime.parse(expirationDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } catch (DateTimeParseException e) {
            throw new NullException();
        }
        return new Bill(name, cardHolder, cardNumber, ldt, Integer.parseInt(cvc), Double.parseDouble(balance));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}
