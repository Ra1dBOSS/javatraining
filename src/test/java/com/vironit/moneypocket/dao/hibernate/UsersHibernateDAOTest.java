package com.vironit.moneypocket.dao.hibernate;

import com.vironit.moneypocket.configuration.DispatcherServletInitializer;
import com.vironit.moneypocket.configuration.HibernateConfiguration;
import com.vironit.moneypocket.configuration.WebAplicationContextConfig;
import com.vironit.moneypocket.dao.UsersDAO;
import com.vironit.moneypocket.model.User;
import com.vironit.moneypocket.model.exceptions.EntityNotFoundException;
import com.vironit.moneypocket.model.exceptions.IncorrectPersonalInformationException;
import com.vironit.moneypocket.model.exceptions.NullException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class})
public class UsersHibernateDAOTest {

    @Autowired
    UsersDAO dao;

    private String expectedLogin;
    private String expectedPassword;
    private String expectedName;
    private String expectedSurname;
    private String expectedLastname;

    @Before
    public void setUp() throws Exception {
        this.expectedLogin = "TestLogin";
        this.expectedPassword = "TestPassword";
        this.expectedName = "TestName";
        this.expectedSurname = "TestSurname";
        this.expectedLastname = "TestLastname";
    }

    @Test
    @Transactional
    public void add() {
        try {
            User user = new User(expectedLogin, expectedPassword, expectedName, expectedSurname, expectedLastname);
            dao.add(user);
            //dao.delete(user);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    @Transactional
    public void getByLogin() {
        List<User> list = new ArrayList<>();
        try {
            list.add(dao.getByLogin(expectedLogin));
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (NullException e) {
            e.printStackTrace();
            fail();
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            fail();
        }

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getLogin(), expectedLogin);
        assertEquals(list.get(0).getName(), expectedName);
        assertEquals(list.get(0).getSurname(), expectedSurname);
        assertEquals(list.get(0).getLastname(), expectedLastname);
    }



    @Test
    @Transactional
    public void getById() {
        try {
            this.dao.add(new User(expectedLogin, expectedPassword, expectedName, expectedSurname, expectedLastname));
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<User> list = new ArrayList<>();
        try {
            list.add(dao.getByLogin(expectedLogin));
            int id = list.get(0).getId();
            list.clear();
            list.add(dao.getById(id));
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (NullException e) {
            e.printStackTrace();
            fail();
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            fail();
        }
        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getLogin(), expectedLogin);
        assertEquals(list.get(0).getName(), expectedName);
        assertEquals(list.get(0).getSurname(), expectedSurname);
        assertEquals(list.get(0).getLastname(), expectedLastname);
    }
/*
    @Test
    @Transactional
    public void update() {
        User user = new User();
        try {
            user = new User(expectedLogin, expectedPassword, expectedName, expectedSurname, expectedLastname);
            this.dao.add(user);

        } catch (Exception e) {}
        expectedLogin = "NewTestLogin";
        expectedPassword = "NewTestPassword";
        expectedName = "NewTestName";
        expectedLastname = "NewTestLastName";
        expectedSurname = "NewTestSurname";
        List<User> list = new ArrayList<>();
        try {
            User user2 = new User(expectedLogin, expectedPassword, expectedName, expectedSurname, expectedLastname);
            user2.setId(user.getId());
            list.add(user2);
            dao.update(list.get(0));
            try {
                dao.delete(list.get(0));
            } catch (Exception e) {}
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (IncorrectPersonalInformationException e) {
            e.printStackTrace();
            fail();
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            fail();
        }

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getLogin(), expectedLogin);
        assertEquals(list.get(0).getName(), expectedName);
        assertEquals(list.get(0).getSurname(), expectedSurname);
        assertEquals(list.get(0).getLastname(), expectedLastname);
    }*/

    @Test
    @Transactional
    public void delete() {
        try {
            this.dao.add(new User(expectedLogin, expectedPassword, expectedName, expectedSurname, expectedLastname));
        } catch (Exception e) {}
        List<User> list = new ArrayList<>();
        try {
            list.add(dao.getByLogin(expectedLogin));
            if (list.size() != 1)
                fail();
            dao.delete(list.get(0));
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        } catch (NullException e) {
            e.printStackTrace();
            fail();
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
            fail();
        }

        assertEquals(list.size(), 1);
        assertEquals(list.get(0).getLogin(), expectedLogin);
        assertEquals(list.get(0).getName(), expectedName);
        assertEquals(list.get(0).getSurname(), expectedSurname);
        assertEquals(list.get(0).getLastname(), expectedLastname);
    }
}