package com.vironit.moneypocket.dao.hibernate;

import com.vironit.moneypocket.configuration.HibernateConfiguration;
import com.vironit.moneypocket.model.Bill;
import com.vironit.moneypocket.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {HibernateConfiguration.class})
@Transactional
public class BillsHibernateDAOTest {

    @Autowired
    @Qualifier("BillsDAO")
    AbstractHibernateDAO<Bill> billHibernateDAO;

    @Test
    public void add() {
        Bill bill = new Bill();
        bill.setName("TestBill");
        bill.setBalance(15);
        bill.setCardNumber("1234123412341234");
        bill.setCardHolder("TestCardHolder");
        bill.setExpirationDate(LocalDateTime.now());
        bill.setCvc(666);
        User user = new User();
        user.setName("TestName");
        user.setLastname("TestLastname");
        user.setLogin("testLogin");
        user.setPassword("TestPassword");
        user.setSurname("TestSurname");
        bill.setUser(user);
        try {
            billHibernateDAO.add(bill);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void getByForeignIdTest() {

        List<Bill> ret = null;

        try {
            ret = billHibernateDAO.getByForeignId(79);
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

        assertNotNull(ret);
    }
}