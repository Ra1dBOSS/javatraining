package com.vironit.moneypocket.model;

import com.vironit.moneypocket.service.implementation.CardValidatorImpl;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CardValidatorImplTest {

    private CardValidatorImpl objectToTest;

    private String[] correctVisaCardNumber = {"4024007137588769",
            "4532397606343787",
            "4485205649991750",
            "4532542142945484",
            "4929906565350749"};

    private String[] correctMasterCardNumber = {"5468798056701603",
            "5144966546040823",
            "5136888359081332",
            "5361769917259816",
            "5567212855829979",};

    private String[] incorrectCardNumber  = {"5467898056701603",
            "113243234554455",
            "51368883590813",
            "536176991a259816",
            "2222212855812121",};

    @Before
    public void setUp() throws Exception {
        objectToTest = new CardValidatorImpl();
    }

    @Test
    public void luhnCheckNull() {
        assertFalse(objectToTest.luhnCheck(null));
    }

    @Test
    public void luhnCheckVisa() {
        for (String cardNumber : correctVisaCardNumber) {
            assertTrue("Correct Visa Card Number " + cardNumber + " get incorrect Luhn check", objectToTest.luhnCheck(cardNumber));
        }
    }

    @Test
    public void luhnCheckMasterCard() {
        for (String cardNumber : correctMasterCardNumber) {
            assertTrue("Correct Master Card Number " + cardNumber + " get incorrect Luhn check", objectToTest.luhnCheck(cardNumber));
        }
    }

    @Test
    public void luhnCheckIncorrectCardNumber() {
        for (String cardNumber : incorrectCardNumber) {
            assertFalse("Inorrect Card Number " + cardNumber + " get correct Luhn check", objectToTest.luhnCheck(cardNumber));
        }
    }
}